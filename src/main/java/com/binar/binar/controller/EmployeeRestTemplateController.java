package com.binar.binar.controller;

import com.binar.binar.entity.Employee;
import com.binar.binar.service.EmployeeRestTemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1/rt/")
public class EmployeeRestTemplateController {

    @Autowired
    public EmployeeRestTemplateService service;

    @GetMapping("/restlist")
    @ResponseBody
    public ResponseEntity<Map> getList() {
        Map c = service.getData();
        return new ResponseEntity<Map>(c, HttpStatus.OK);
    }

    @PostMapping("/restsave")
    public ResponseEntity<Map> save(@Valid @RequestBody Employee objModel) {
        Map map = new HashMap();
        Map obj = service.insert(objModel);
        map.put("Request =", objModel);
        map.put("Response =", obj);
        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }

    @PutMapping("/restupdate")
    public ResponseEntity<Map> update(@Valid @RequestBody Employee objModel) {

        Map map = new HashMap();
        Map c = service.update(objModel);

        map.put("data", c);
        return new ResponseEntity<Map>(c, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Map> update(@PathVariable("id") Long id) {
        Map obj = service.delete(id);

        return new ResponseEntity<Map>(obj, HttpStatus.OK);
    }

}
