package com.binar.binar.controller.thymeleaf;

import com.binar.binar.entity.Employee;
import com.binar.binar.exception.ResourceNotFoundException;
import com.binar.binar.service.EmployeeThymeleafService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/v1/view/")
public class EmployeeThymeleafController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final int ROW_PER_PAGE = 5;

    @Autowired
    private EmployeeThymeleafService employeeService;

//    @Value("${msg.title}")
//    private String title;

    @GetMapping(value = {"/", "/index"})
    public String index(Model model) {
        model.addAttribute("title", "Welcome");
        return "index";
    }

    @GetMapping(value = "/employees")
    public String getEmployees(Model model,
                              @RequestParam(value = "page", defaultValue = "1") int pageNumber) {
        List<Employee> employees = employeeService.findAll(pageNumber, ROW_PER_PAGE);

        long count = employeeService.count();
        boolean hasPrev = pageNumber > 1;
        boolean hasNext = (pageNumber * ROW_PER_PAGE) < count;
        model.addAttribute("employees", employees);
        model.addAttribute("hasPrev", hasPrev);
        model.addAttribute("prev", pageNumber - 1);
        model.addAttribute("hasNext", hasNext);
        model.addAttribute("next", pageNumber + 1);
        return "employee-list";
    }

    @GetMapping(value = {"/employees/add"})
    public String showAddEmployee(Model model) {
        Employee emp = new Employee();
        model.addAttribute("add", true);
        model.addAttribute("employee", emp);
        return "employee-edit";
    }

    @PostMapping(value = "/employess/add")
    public String addEmployee(Model model,
                             @ModelAttribute("employee") Employee employee) {
        try {
            Employee newEmployee = employeeService.save(employee);
            return "redirect:/employees/" + String.valueOf(newEmployee.getId());
        } catch (Exception ex) {
            // log exception first,
            // then show error
            String errorMessage = ex.getMessage();
            logger.error(errorMessage);
            model.addAttribute("errorMessage", errorMessage);
            model.addAttribute("add", true);
            return "employee-edit";
        }
    }

    @GetMapping(value = {"/employees/{employeeId}/edit"})
    public String showEditEmployee(Model model, @PathVariable long employeeId) {
        Employee employee = null;
        try {
            employee = employeeService.findById(employeeId);
        } catch (ResourceNotFoundException ex) {
            model.addAttribute("errorMessage", "Employee not found");
        }
        model.addAttribute("add", false);
        model.addAttribute("employee", employee);
        return "employee-edit";
    }

    @PostMapping(value = {"/employees/{employeeId}/edit"})
    public String updateEmployee(Model model,
                                @PathVariable long employeeId,
                                @ModelAttribute("employee") Employee employee) {
        try {
            employee.setId(employeeId);
            employeeService.update(employee);
            return "redirect:/employees/" + String.valueOf(employee.getId());
        } catch (Exception ex) {
            // log exception first,
            // then show error
            String errorMessage = ex.getMessage();
            logger.error(errorMessage);
            model.addAttribute("errorMessage", errorMessage);

            model.addAttribute("add", false);
            return "employee-edit";
        }
    }

    @GetMapping(value = "/employees/{employeeId}")
    public String getEmployeeById(Model model, @PathVariable long employeeId) {
        Employee employee = null;
        try {
            employee = employeeService.findById(employeeId);
        } catch (ResourceNotFoundException ex) {
            model.addAttribute("errorMessage", "Employee not found");
        }
        model.addAttribute("employee", employee);
        return "employee";
    }

    @GetMapping(value = {"/employees/{employeeId}/delete"})
    public String showDeleteEmployeeById(
            Model model, @PathVariable long employeeId) {
        Employee employee = null;
        try {
            employee = employeeService.findById(employeeId);
        } catch (ResourceNotFoundException ex) {
            model.addAttribute("errorMessage", "Employee not found");
        }
        model.addAttribute("allowDelete", true);
        model.addAttribute("employee", employee);
        return "employee";
    }

    @PostMapping(value = {"/employees/{employeeId}/delete"})
    public String deleteEmployeeById(
            Model model, @PathVariable long employeeId) {
        try {
            employeeService.deleteById(employeeId);
            return "redirect:/employees";
        } catch (ResourceNotFoundException ex) {
            String errorMessage = ex.getMessage();
            logger.error(errorMessage);
            model.addAttribute("errorMessage", errorMessage);
            return "employee";
        }
    }


}
