package com.binar.binar.repository;

import com.binar.binar.entity.Employee;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface EmployeeThymeleafRepo extends PagingAndSortingRepository<Employee, Long>,
        JpaSpecificationExecutor<Employee> {
}