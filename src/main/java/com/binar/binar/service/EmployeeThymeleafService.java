package com.binar.binar.service;

import com.binar.binar.entity.Employee;
import com.binar.binar.exception.BadResourceException;
import com.binar.binar.exception.ResourceAlreadyExistsException;
import com.binar.binar.exception.ResourceNotFoundException;
import com.binar.binar.repository.EmployeeThymeleafRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import org.springframework.data.domain.Pageable;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class EmployeeThymeleafService {

    @Autowired
    private EmployeeThymeleafRepo repo;

    private boolean existsById(Long id) {
        return repo.existsById(id);
    }

    public Employee findById(Long id) throws ResourceNotFoundException {
        Employee employee = repo.findById(id).orElse(null);
        if (employee==null) {
            throw new ResourceNotFoundException("Cannot find Contact with id: " + id);
        }
        else return employee;
    }

    public List<Employee> findAll(int pageNumber, int rowPerPage) {
        List<Employee> employees = new ArrayList<>();
        Pageable sortedByIdAsc = PageRequest.of(pageNumber - 1, rowPerPage,
                Sort.by("id").ascending());
        repo.findAll(sortedByIdAsc).forEach(employees::add);
        return employees;
    }

    public Employee save(Employee employee) throws BadResourceException, ResourceAlreadyExistsException {
        if (!StringUtils.isEmpty(employee.getName())) {
            if (employee.getId() != null && existsById(employee.getId())) {
                throw new ResourceAlreadyExistsException("Contact with id: " + employee.getId() +
                        " already exists");
            }
            return repo.save(employee);
        }
        else {
            BadResourceException exc = new BadResourceException("Failed to save contact");
            exc.addErrorMessage("Contact is null or empty");
            throw exc;
        }
    }

    public void update(Employee employee)
            throws BadResourceException, ResourceNotFoundException {
        if (!StringUtils.isEmpty(employee.getName())) {
            if (!existsById(employee.getId())) {
                throw new ResourceNotFoundException("Cannot find Contact with id: " + employee.getId());
            }
            repo.save(employee);
        }
        else {
            BadResourceException exc = new BadResourceException("Failed to save contact");
            exc.addErrorMessage("Contact is null or empty");
            throw exc;
        }
    }

    public void deleteById(Long id) throws ResourceNotFoundException {
        if (!existsById(id)) {
            throw new ResourceNotFoundException("Cannot find contact with id: " + id);
        }
        else {
            repo.deleteById(id);
        }
    }

    public Long count() {
        return repo.count();
    }
}
