package com.binar.binar.service.impl;

import com.binar.binar.entity.Employee;
import com.binar.binar.entity.EmployeeTraining;
import com.binar.binar.repository.EmployeeRepo;
import com.binar.binar.repository.EmployeeTrainingRepo;
import com.binar.binar.service.EmployeeTrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class EmployeeTrainingImpl implements EmployeeTrainingService {

    @Autowired
    public EmployeeTrainingRepo repoEmployeeTraining;

    @Autowired
    public EmployeeRepo repoEmployee;

    @Override
    public Map insert(EmployeeTraining employeeTraining) {
        Map map = new HashMap();
        try {
            Employee employee = repoEmployee.getById(employeeTraining.getEmployee().getId());
            employeeTraining.setEmployee(employee);
            EmployeeTraining obj = repoEmployeeTraining.save(employeeTraining);
            map.put("data", obj);
            map.put("statusCode", "200");
            map.put("statusMessage", "Sukses");
            return map;// respon

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;
        }
    }

    @Override
    public Map getData() {
        List<EmployeeTraining> list = new ArrayList<EmployeeTraining>();
        Map map = new HashMap();
        try {

            list = repoEmployeeTraining.getList();
            map.put("data", list);//data
            map.put("statusCode", 200);
            map.put("statusMessage", "Get Sukses");
            return map;//success

        } catch (Exception e) {
            e.printStackTrace();
            map.put("statusCode", "500");
            map.put("statusMessage", e);
            return map;// eror
        }
    }


}
